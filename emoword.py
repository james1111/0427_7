#!/usr/bin/python
#   stdin = tweet CSV file such as "msgraw_sample.txt"
#   emoticon to use is the first command line argument

import csv
import sys
import re
import string
import operator

emo2words = {}

emo = sys.argv[1]


fp = open("emoticon.csv", "r")
for line in fp:
    line = line.rstrip()
    values = line.split("\t")
    # print(values[1])


    emo2words[values[1]] = {}


fp.close()



file = open("msgraw_sample.txt","r")

lines = file.readlines();

for line in lines:

    for e in emo2words.keys():

        if line.__contains__(e):

            words = line.split()

            for w in words:

                if w not in emo2words[e].keys():

                    emo2words[e][w] = 1

                else:

                    emo2words[e][w] = emo2words[e][w] + 1






for e in emo2words.keys():

    sorted_emo = sorted(emo2words[e].items(), key = operator.itemgetter(1))

    i = 0;
    print(e)
    for w in emo2words[e].keys():

        if i > 20 :
            break;
        else:
            print emo2words[e][w]
            i = i + 1



file.close()
