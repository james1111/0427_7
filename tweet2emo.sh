#!/usr/bin/env bash

tr -cd '[:alnum:][:punct:][:space:]'   < msgraw_sample.txt > line.txt

perl -p -e 's/\s+/\n/g;'   line.txt > output.txt


grep -E ":-)|;-)|:)|:-]:]|:-3|:->|:>|8-)|8)|:-}|:}|:o)|:c)|:^)|=]|=)|:‑D|:D|8‑D8D|x‑D|xD|X‑D|XD|=D|=3|B^D|:(|:‑c|:c|:‑<|:<|:{|:@|:'‑)|:')|D‑':||D:<|D:|D8|D;|D=|DX|:‑O|:‑o|:o|:-0|8‑0|>:O|:-*|:×|;‑)|;)|*-)|*)|;‑]|;]|;^)|:‑,|;D|:‑P|:P|X‑P|XP|x‑p|xp|:‑p|:p|:‑Þ|:Þ|:‑þ|:þ|:‑b|:b|d:|:$|:‑X|:X|:‑#|:#|:‑&|:&|O:‑)|O:)|0:‑3|0:‑)|0:)|>:‑)|>:)|}:‑)|}:)|3:‑)|3:)|>;)|:‑J|#‑)"   output.txt > result.txt


perl -p -e 's/^\s+//; s/ /\t/; ' < result.txt |sort |uniq -c > final.csv
