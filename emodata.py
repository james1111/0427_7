#!/usr/bin/python
#   stdin = tweet CSV file such as "msgraw_sample.txt"
#   reads "emoticon.csv" to fetch emoticons

import csv
import sys
import re
import string


from nltk import wordpunct_tokenize
from nltk.corpus import stopwords

emo2language = {}

emo = sys.argv[1]


fp = open("emoticon.csv", "r")
for line in fp:
    line = line.rstrip()
    values = line.split("\t")
    # print(values[1])

    emo2language[values[1]] = {}


fp.close()



file = open("msgraw_sample.txt","r")

lines = file.readlines();
languages_ratios = {}
for line in lines:
    tokens = wordpunct_tokenize(line)

    words = [word.lower() for word in tokens]
    for language in stopwords.fileids():

        stopwords_set = set(stopwords.words(language))

        words_set = set(words)

        common_elements = words_set.intersection(stopwords_set)

        languages_ratios[language] = len(common_elements)  # language "score"

        most_rated_language = max(languages_ratios, key=languages_ratios.get)

    for e in emo2language.keys():
        if line.__contains__(e):

            if most_rated_language not in emo2language[e].keys():
                emo2language[e][most_rated_language]  = 1
            else:
                emo2language[e][most_rated_language] = emo2language[e][most_rated_language] + 1



print(emo2language)






