#!/usr/bin/python


import ast
import csv
from sklearn import  linear_model
from sklearn.metrics import mean_squared_error, r2_score

import numpy as np
from scipy import stats

Y_train = np.zeros(101,dtype=float)
X_1_train = np.zeros((101,4))
X_2_train = np.zeros((101,3))
Y_test = np.zeros(21)
X_1_test = np.zeros((21,4))
X_2_test = np.zeros((21,3))

X_1 = []
X_2 = []
X_3 = []
X_4 = []


with open('train.csv', 'rb') as csvfile:
    datareader = csv.reader(csvfile, delimiter=',')
    next(datareader)
    i = 0;
    for r in datareader:


        Y_train[i] = float(r[0])
        X_1_train[i][0] = float( r[1])

        X_1_train[i][1] =float( r[2])
        X_1_train[i][2] = float(r[3])
        X_1_train[i][3] = float(r[4])

        X_2_train[i][0] = float(r[2])
        X_2_train[i][1] = float(r[3])
        X_2_train[i][2] =float( r[4])

        X_1.append(float(r[1]))
        X_2.append(float(r[2]))
        X_3.append(float(r[3]))
        X_4.append(float(r[4]))

        i = i + 1

print("Task B.1")
K = np.zeros(4)
P = np.zeros(4)

K[0],P[0] = stats.normaltest(X_1)
K[1],P[1] = stats.normaltest(X_2)
K[2],P[2] = stats.normaltest(X_3)
K[3],P[3] = stats.normaltest(X_4)

select = np.argmin(P)

print "The X_{} is the most likely samples drawn from normal distributions.".format(select + 1)




regr_1 = linear_model.LinearRegression()
regr_2 = linear_model.LinearRegression()

regr_1.fit(X_1_train,Y_train)

regr_2.fit(X_2_train,Y_train)





with open('test.csv', 'rb') as csvfile:
    datareader = csv.reader(csvfile, delimiter=',')
    next(datareader)
    i = 0
    for r in datareader:

        Y_test[i] = float(r[0])
        X_1_test[i][0] = float(r[1])
        X_1_test[i][1] =float( r[2])
        X_1_test[i][2] = float(r[3])
        X_1_test[i][3] = float(r[4])

        X_2_test[i][0] = float(r[2])
        X_2_test[i][1] =float( r[3])
        X_2_test[i][2] = float(r[4])
        i = i  + 1


Y_1_predict = regr_1.predict(X_1_test)
Y_2_predict = regr_2.predict(X_2_test)

r_2_score_1 = r2_score(Y_test,Y_1_predict)
r_2_score_2 = r2_score(Y_test,Y_2_predict)

# if Y_1_predict > Y_2_predict:
#     print("The first one has higher R square value")
# else:
#     print("The second one has higher R square value")


print("Task B.2")
print "The {} one has higher r-square value".format("first" if r_2_score_1 > r_2_score_2 else "second")

print("Task B.3")

print("The first MSE is {}".format(mean_squared_error(Y_1_predict,Y_test)))
print("The second MSE is {}".format(mean_squared_error(Y_2_predict,Y_test)))